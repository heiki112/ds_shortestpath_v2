package shortestPath;
import java.util.ArrayList;


public class Way {  // Used only when importing data.
    public ArrayList<Long> nodes = new ArrayList<Long>();
    String type;
    boolean oneway = false;
    
    public void addNode(long id) {
        nodes.add(id);
    }
    
    public ArrayList<Long> getNodes() {
        return nodes;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String s) {
        type = s;
    }
    
    public boolean isOneway() {
        return oneway;
    }
    
    public void setOneway() {
        oneway = true;
    }
}
