package shortestPath;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import dataStructures.BinaryHeap;


public class ShortestPath {
    
    public static final double R = 6372.8; // In kilometers, used for distance calculation.
    SaxHandler handler;
    
    
    public ShortestPath(String file) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            InputStream xmlInput = new FileInputStream(file);

            SAXParser saxParser = factory.newSAXParser();
            handler = new SaxHandler();
            saxParser.parse(xmlInput, handler);
            
        } catch (Throwable err) {
            err.printStackTrace ();
        }
    }

    public String shortestPath(double lat1, double lon1, double lat2, double lon2) {
        MyNode a = getNearest(lat1, lon1);
        MyNode b = getNearest(lat2, lon2);
        return aStar(a, b);
    }

    public Double[] getBounds() {
        return handler.mapBounds;
    }

    private MyNode getNearest(double lat, double lon) {
        double[] coords = new double[]{lat, lon};
        return handler.nodeTree.nearestNeighbor(coords, 1, false).get(0).value;
    }

    private String aStar(MyNode start, MyNode end) {  // A* algorithm implementation, based on wikipedia pseudocode.
        HashSet<Long> closedSet = new HashSet<Long>();
        BinaryHeap openSet = new BinaryHeap();
        Map<MyNode, MyNode> cameFrom = new HashMap<MyNode, MyNode>();

        openSet.add(start);
        start.setGscore(0);
        start.setFscore(start.getGscore() + start.distanceFrom(end));

        MyNode current;
        double tentative_g_score;
        while(!openSet.isEmpty()) {
            current = openSet.remove();
            if(current == end) {
                return reconstructPathCoordinates(current, start, cameFrom);
            }
            closedSet.add(current.getId());
            for(int i = 0; i < current.getConnections().size(); i++) {
                long id = current.getConnections().get(i);
                MyNode n = handler.nodes.get(id);
                if(n == null) {
                    current.getConnections().remove(id);
                    i--;
                }
                else if(!(closedSet.contains(n.getId()))) {
                    tentative_g_score = current.getGscore() + current.distanceFrom(n);
                    if(tentative_g_score < n.getGscore() || !(openSet.contains(n))) {
                        cameFrom.put(n, current);
                        n.setGscore(tentative_g_score);
                        n.setFscore(n.getGscore() + n.distanceFrom(end)/n.getPriority());
                        openSet.remove(n);
                        openSet.add(n);
                    }
                }
            }
        }
        return "Failure";
    }

    private String reconstructPathCoordinates(MyNode is, MyNode start, Map<MyNode, MyNode> cameFrom) {
        double distance = 0;
        LinkedList<MyNode> path = new LinkedList<MyNode>();
        path.add(is);
        MyNode current = is;
        String result = "";
        while(current != start) {
            distance += current.distanceFrom(cameFrom.get(current));
            current = cameFrom.get(current);
            path.add(current);
        }
        for(int i = 0, size = path.size(); i < size; i++) {
            result += (path.get(path.size()-i-1).getLat() + "," + path.get(path.size()-i-1).getLon());
            if(i < path.size()-1) {
                result += ",";
            }
        }
        result += ";" + distance;
        return result;
    }
}
