package shortestPath;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import dataStructures.KdTree;

import java.util.*;

public class SaxHandler extends DefaultHandler {

    public Double[] mapBounds = new Double[4];
    public Map<Long, MyNode> nodes = new HashMap<Long, MyNode>();
    KdTree.WeightedSqrEuclid<MyNode> nodeTree = new KdTree.WeightedSqrEuclid<MyNode>(2, null);

    private Stack<String> elementStack = new Stack<String>();
    private Stack<Object> objectStack  = new Stack<Object>();
    Map<String, Double> priorities = new HashMap<String, Double>();
    
    public void startDocument() throws SAXException {
        priorities = new HashMap<String, Double>();
        priorities.put("motorway", 2.3);
        priorities.put("motorway_link", 2.3);
        priorities.put("trunk", 2.2);
        priorities.put("trunk_link", 2.2);
        priorities.put("primary", 2.1);
        priorities.put("primary_link", 2.1);
        priorities.put("secondary", 2.0);
        priorities.put("secondary_link", 2.0);
    }

    public void startElement(String uri, String localName,
            String qName, Attributes attributes) throws SAXException {

        this.elementStack.push(qName);

        if("node".equals(qName)){
            long attId = Long.parseLong(attributes.getValue(0));
            double attLat = Double.parseDouble(attributes.getValue(6));
            double attLon = Double.parseDouble(attributes.getValue(7));
            MyNode node = new MyNode(attId, attLat, attLon);
            this.nodes.put(attId, node);
        } else if("way".equals(qName)){
            this.objectStack.push(new Way());
        } else if("nd".equals(qName)) {
            if("way".equals(currentElementParent())) {
                Way w = (Way) currentObject();
                w.addNode(Long.parseLong(attributes.getValue(0)));
            }
        } else if("tag".equals(qName)) {
            if("way".equals(currentElementParent())) {
                String tagType = attributes.getValue(0);
                Way w = (Way) currentObject();
                if("highway".equals(tagType)) {
                    w.setType(attributes.getValue(1));
                }
                else if("oneway".equals(tagType)) {
                    if("yes".equals(attributes.getValue(1))) {
                        w.setOneway();
                    }
                }
            }
        } else if("bounds".equals(qName)) {
            for(int i = 0; i < 4; i++) {
                mapBounds[i] = Double.parseDouble(attributes.getValue(i));
            }
        }
    }

    public void endElement(String uri, String localName,
            String qName) throws SAXException {

        this.elementStack.pop();

        if("way".equals(qName)){
            Way w = (Way) currentObject();
            ArrayList<Long> wayNodes = w.getNodes();
            for(int i = 0; i < wayNodes.size()-1; i++) {
                MyNode n1 = nodes.get(wayNodes.get(i));
                MyNode n2 = nodes.get(wayNodes.get(i+1));
                n1.setPriority(priorities.get(w.getType()));
                n2.setPriority(priorities.get(w.getType()));
                n1.connect(n2.getId());
                if(!w.isOneway()) {
                    n2.connect(n1.getId());
                }
            }
            this.objectStack.pop();
        }
    }

    public void characters(char ch[], int start, int length)
            throws SAXException {
        // Nothing to do here. All data is in element attributes.
    }
    
    private Object currentObject() {
        return this.objectStack.peek();
    }

    /*private String currentElement() {   // Not needed
        return this.elementStack.peek();
    }*/

    private String currentElementParent() {
        if(this.elementStack.size() < 2) return null;
        return this.elementStack.get(this.elementStack.size()-2);
    }
    
    @SuppressWarnings("rawtypes")
    public void endDocument() throws SAXException {
        Iterator it = nodes.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if(((MyNode) pair.getValue()).connectedTo.size() < 1) {
                it.remove();
            }
            else {
                MyNode node = (MyNode) pair.getValue();
                double[] coords = new double[]{node.getLat(), node.getLon()};
                nodeTree.addPoint(coords, node);
            }
        }
    }

} 