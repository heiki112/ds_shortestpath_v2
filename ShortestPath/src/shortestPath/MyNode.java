package shortestPath;

import java.util.LinkedList;


public class MyNode implements Comparable<MyNode> {
    long id;
    Double priority;
    double lat;
    double lon;
    double fscore;	// Used by a* algorithm
    double gscore;	//
    LinkedList<Long> connectedTo;

    public MyNode() {
        connectedTo = new LinkedList<Long>();
        fscore = 0;
        gscore = 0;
    }
    
    public MyNode(long num, double latitude, double longditude) {
        id = num;
        lat = latitude;
        lon = longditude;
        connectedTo = new LinkedList<Long>();
        fscore = 0;
        gscore = 0;
    }
    
    public void setId(long l) {
        id = l;
    }

    public long getId() {
        return id;
    }

    public void setLatLon(double latitude, double longditude) {
        lat = latitude;
        lon = longditude;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }

    public void setGscore(double n) {
        gscore = n;
    }

    public double getGscore() {
        return gscore;
    }

    public void setFscore(double n) {
        gscore = n;
    }

    public double getFscore() {
        return gscore;
    }
    
    public void setPriority(Double n) {
        if(n != null) {
            priority = n;
        } else {
            priority = 1.0;
        }
    }

    public Double getPriority() {
        return priority;
    }


    public LinkedList<Long> getConnections() {
        return connectedTo;
    }

    public boolean isConnected(long v) {
        if(connectedTo.contains(v)) {
            return true;
        }
        else {
            return false;
        }
    }

    public void connect(long v) {
        if(!isConnected(v)) {
            connectedTo.add(v);
        }
    }

    public double distanceFrom(MyNode n) {	// Haversine formula
        double lat1 = Math.toRadians(lat);
        double lat2 = Math.toRadians(n.getLat());
        double dLat = Math.toRadians(n.getLat() - lat);
        double dLon = Math.toRadians(n.getLon() - lon);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return ShortestPath.R * c;
    }

    public double distanceFrom(double latOther, double lonOther) {	// Haversine formula
        double lat1 = Math.toRadians(lat);
        double lat2 = Math.toRadians(latOther);
        double dLat = Math.toRadians(latOther - lat);
        double dLon = Math.toRadians(lonOther - lon);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return ShortestPath.R * c;
    }

    @Override
    public int compareTo(MyNode arg0) {
        if (fscore > arg0.getFscore()) {
            return -1;
        } else if (fscore < arg0.getFscore()) {
            return 1;
        } else return 0;
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof MyNode)) return false;
        MyNode otherMyClass = (MyNode)other;
        if(id == otherMyClass.getId()) {
            return true;
        }
        else return false;
    }

}
