package servlet;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import shortestPath.ShortestPath;

/**
 * Servlet implementation class ShortestPath
 */
@WebServlet("/ShortestPath")
public class ShortestPathServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ShortestPath sp;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShortestPathServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init() {
        String path = this.getServletContext().getRealPath("/WEB-INF/map.osm");
        sp = new ShortestPath(path);
    }
    
    private String processPathRequest(HttpServletRequest request) {
        double lat1 = Double.valueOf(request.getParameter("lat1"));
        double lon1 = Double.valueOf(request.getParameter("lon1"));
        double lat2 = Double.valueOf(request.getParameter("lat2"));
        double lon2 = Double.valueOf(request.getParameter("lon2"));
        String solution = sp.shortestPath(lat1, lon1, lat2, lon2);
        return solution;
    }
    
    private String getBounds(HttpServletRequest request) {
        String bounds = "";
        for(Double dbl : sp.getBounds()) {
            bounds += String.valueOf(dbl);
            bounds += ", ";
        }
        bounds = bounds.substring(0, bounds.length()-2); // Remove the last ", "
        return bounds;
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String type = request.getParameter("type");
        switch(type) {
            case "0":
                response.getWriter().write(processPathRequest(request));
                break;
            case "1":
                response.getWriter().write(getBounds(request));
                break;
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

}
