package dataStructures;
import java.util.Comparator;
import java.util.PriorityQueue;

import shortestPath.MyNode;


public class BinaryHeap extends PriorityQueue<MyNode> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;


    public static class NodeCostComparator implements Comparator<MyNode> {

        @Override
        public int compare(MyNode object1, MyNode object2) {
            if(object1.getFscore() > object2.getFscore()) {
                return 1;
            }
            else if(object1.getFscore() == object2.getFscore()) {
                return 0;
            }
            else if(object1.getFscore() < object2.getFscore()) {
                return -1;
            }else{
                return 0;
            }

        }

    }
    private final static int DEFAULT_CAPACITY = 13;
    private static final NodeCostComparator mComparator = new NodeCostComparator();

    public BinaryHeap() {
        super(DEFAULT_CAPACITY, mComparator);
    }



}