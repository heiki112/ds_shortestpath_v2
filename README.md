Running live on:
**http://52.28.104.167/ShortestPath/**

Client-server application for finding the shortest (vehicle) path between two points. It uses OpenStreetmap data preprocessed with [Osmosis](http://wiki.openstreetmap.org/wiki/Osmosis). A* algorithm has been implemented to find the shortest path. [This](https://bitbucket.org/rednaxela/knn-benchmark/src/de97871b1569fdc63b30ac60da210352ac935a04/ags/utils/dataStructures/trees/secondGenKD/KdTree.java?at=default) Kd-tree implementation is used in the project for nearest-neighbour search.

Demo is running on Amazon EC2 microinstance, using the map of Estonia. For size considerations the map.osm file included in source code is of Tartu.


  *Heiki Pärn*